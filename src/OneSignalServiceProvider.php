<?php

namespace OneSignal;

use Illuminate\Support\ServiceProvider;
use OneSignal\foundation\OneSignalClient;

class OneSignalServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__ . '/config/onesignal.php';

        $this->publishes([$configPath => config_path('onesignal.php')], 'onesignal');
        $this->mergeConfigFrom($configPath, 'onesignal');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/views', 'OneSignalView');

        $this->loadMigrationsFrom(__DIR__.'/migrations');

        if ( class_exists('Laravel\Lumen\Application') ) {
            $this->app->configure('onesignal');
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('onesignal', function ($app) {
            $config = isset($app['config']['services']['onesignal']) ? $app['config']['services']['onesignal'] : null;
            if (is_null($config)) {
                $config = $app['config']['onesignal'] ?: $app['config']['onesignal::config'];
            }

            $client = new OneSignalClient($config['app_id'], $config['rest_api_key'], $config['user_auth_key']);

            return $client;
        });
    }

    public function provides() {
        return ['onesignal'];
    }


}
