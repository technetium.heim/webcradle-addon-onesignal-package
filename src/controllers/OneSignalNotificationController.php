<?php

namespace OneSignal\controllers;

use App\Http\Controllers\CradleController;
use OneSignal;
use Illuminate\Http\Request;
use Auth;

class OneSignalNotificationController extends CradleController
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function showNotificationConfig()
    {
        return view('OneSignalView::notification_config', $this->setTitle('Notification Config')); 
    }

    public function showNotificationForm()
    {
        return view('OneSignalView::notification_send', $this->setTitle('Send Notification')); 
    }

    public function showNotificationList(Request $request)
    {
        $current_page = $request->page? $request->page: 1;
        $perpage = 10;
        $data = Json_decode(OneSignal::getNotificationsList(  $perpage , ($current_page - 1)*$perpage ));   
        $data->page_number = (int) ceil($data->total_count / $perpage );
        $data->current_page =  $current_page;
        $data->start_index =  ($current_page - 1)*$perpage + 1;

        return view('OneSignalView::notification_list', $this->setTitle('Notification Sent'))->withData($data); 
    }

    public function sendNotification(Request $request)
    {
        $title =  $request->title;
        $msg = $request->message;
        $url = $request->url ? $request->url: null;
        $schedule = $request->schedule ? $request->schedule: null;
        // OneSignal::editPlayer([ 'id' => '443c0b6a-2773-4d64-8942-e07fd810d446', 'tags' => [ 'condo'=>'mine'] ]);

        $errors = [];

        if ( $request->schedule == 'scheduled') {
            if ( !$request->date)   {
                $errors['date'] = 'Enter A Date';
            }
            if ( !$request->time)   {
                $errors['time'] = 'Enter A Time';
            }
            
            $schedule = $request->date ." ". $request->time;
            $schedule = \Carbon\Carbon::createFromFormat('Y-d-m H:i',$schedule)->setTimezone('UTC')->toDateTimeString();
        }else{
            $schedule = null;
        }

        // send notification to all user
        if ( $request->audience == 'all') {
            if ( empty($errors) ) OneSignal::sendNotificationToAll( $title, $msg, $url , null, null, $schedule );         
        }

        if ( $request->audience == 'segment') {
            if ( !$request->segment )  {
                $errors['segment'] = 'Enter A Segment';
            }
                
             if ( empty($errors) )  OneSignal::sendNotificationToSegment( $title, $msg, $request->segment, $url , null, null, $schedule);

        }

        if ( $request->audience == 'user') {
            if ( !$request->user_id )  {
                $errors['user_id'] = 'Enter An User ID';
                
            }
                
             if ( empty($errors) )  OneSignal::sendNotificationToUser( $title, $msg, $request->user_id , $url , null, null, $schedule );
                   
        }

        if ($errors) {
            return redirect()->back()
                    ->withInput($request->input())
                    ->withErrors($errors);
        }else{
            $request->session()->flash('p_status', 'notification is send');
        }

      

        return redirect()->back();

        // dd($request->all());

    }

    public function linkOnesignal(Request $request) {
        OneSignal::connectUserOnesignal($request->onesignal_id);       
        return response()->json(['success' => 'success']);
    }

}
