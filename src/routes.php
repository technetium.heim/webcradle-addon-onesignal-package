<?php

Route::group(['middleware' => 'web'], function() { // middleware web required for error share
	Route::prefix(Cradle::config('project.doorbell.admin'))->group(function(){
		Route::get('/notification/config', 'OneSignal\controllers\OneSignalNotificationController@showNotificationConfig')->name('notifications.config');

		Route::get('/notification/sent', 'OneSignal\controllers\OneSignalNotificationController@showNotificationList')->name('notifications.list');

		Route::get('/notification/send', 'OneSignal\controllers\OneSignalNotificationController@showNotificationForm')->name('notifications.form');
		Route::post('/notification/send', 'OneSignal\controllers\OneSignalNotificationController@sendNotification')->name('notifications.send');

		Route::post('/onesignal/linkid', 'OneSignal\controllers\OneSignalNotificationController@linkOnesignal')->name('onesignal.link');

	});
});