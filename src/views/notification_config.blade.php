<!-- layout -->
    @extends('OneSignalView::notification_base')

    @section('notification-content')
    	@component('WCView::general.components.panels.main')
            @slot('title','One Signal Notification Config')
            @slot('content')
                <div class="padding">
                    <div class="row" style="min-height: 30px;">
                        <div class="col-xs-5 col-md-3">
                            App ID
                        </div>
                        <div class="col-xs-7 col-md-9">
                            {{ config('onesignal.app_id') }}
                        </div>
                    </div>
                    <div class="row" style="min-height: 30px;">
                        <div class="col-xs-5 col-md-3">
                            REST API Key
                        </div>
                        <div class="col-xs-7 col-md-9">
                            {{ config('onesignal.rest_api_key') }}
                        </div>
                    </div>
                    <div class="row" style="min-height: 30px;">
                        <div class="col-xs-5 col-md-3">
                            User Auth Key
                        </div>
                        <div class="col-xs-7 col-md-9">
                            {{ config('onesignal.user_auth_key') }}
                        </div>
                    </div>
    

                </div>

                <div>
                    For more functions, Please Login to OneSignal Dashboard.
                    <br>
                    <a href="https://onesignal.com/" target="_blank">Goto OneSignal</a>
                </div>
            @endslot
        @endcomponent

    	
    @endsection

