{{--  
link for include : @include('OneSignalView::check_onesignal_id') 
--}}
<!-- Page for java -->

@if( Auth::check() )
	<script type="text/javascript">
		if ("Android" in window){
			var onesignal = Android.getOneSignalID();
			os_ids = [];
			@foreach( Auth::user()->onesignals()->get()->pluck('onesignal_id')->toArray() as $os_id )
				os_ids.push( "{{ $os_id}}" );
			@endforeach

			if( onesignal != null ){
		
				if (  $.inArray( onesignal , os_ids) == -1  ){
					// send ajax back to save the key
					$.ajax({
				        method: "POST",
				        url: "{{ route('onesignal.link') }}",
				        data: {
				        	"_token": "{{ csrf_token() }}",
				        	'onesignal_id' : onesignal
				        },
				        dataType: 'json',
				        success: function(response_data) {
				        	// alert(response_data.success)
				        },
				        error: function(data) {
				            var errors = data.responseJSON;
				        }
				    });
				}
			}
		}
	</script>


@endif