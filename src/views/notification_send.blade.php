<!-- layout -->
    @extends('OneSignalView::notification_base')

    @section('notification-content')
    	@component('WCView::general.components.panels.main')
            @slot('title','Send OneSignal Notification')
            @slot('content')
                @if( Session::has('p_status') )
                    <div class="alert alert-success">
                        <strong>Notification Sent.</strong>
                    </div>
                @endif
                @php
                    $form = FForm::makeForm();
                    $form->setURL( route('notifications.send') );
                    $form->setMethod( 'POST' );
                    $form->offAuto();
                    $form->newInput('audience', 'dropdown' )
                        ->addOptions( ['all' => 'Everyone', 'segment' => 'Segment', 'user' => 'User' ] )
                        ->setProp( 'required' )
                        ;
                    $form->newInput('segment', 'text' )
                        ->setLabel( 'Segment_Name')
                        ->setProp('inithide')
                        ;
                    $form->newInput('user_id', 'text' )
                        ->setLabel( 'User_id')
                        ->setProp('inithide')
                        ;
                    $form->newInput('title', 'text' )
                        ->setProp( 'required' )
                        ->unsetProp( 'autocomplete' )
                        ;
                    $form->newInput('message', 'textarea' )
                        ->setProp( 'required' )
                        ;
                    $form->newInput('url', 'text' )
                        ->setLabel( 'Url (Redirect when clicked)')
                        ;
                    $form->newInput('schedule', 'radio-inline' )
                        ->addOptions( ['now' => 'Send Now', 'scheduled' => 'Scheduled' ] )
                        ->setProp( 'required' )
                        ;
                    $form->newInput('date', 'date' )
                        ->setLabel( 'Date (Schedule)')
                        ->setProp('inithide')
                        ;
                    $form->newInput('time', 'time' )
                        ->setLabel( 'Time (Schedule)')
                        ->setProp('inithide')
                        ;

                    echo $form->render();
               @endphp
            @endslot
        @endcomponent

    	
    @endsection

    @section('page-script-bottom')
        <script type="text/javascript">
            $(document).ready(function(){

                $('select[name="audience"]').change( function() {
                    if( $(this).val() == 'all'  ){
                        $('#input-container-segment').addClass('hidden');
                        $('#input-container-user_id').addClass('hidden');
                    }

                    if( $(this).val() == 'segment'  ){
                        $('#input-container-segment').removeClass('hidden');
                        $('#input-container-user_id').addClass('hidden'); 
                    }

                    if( $(this).val() == 'user'  ){
                        $('#input-container-segment').addClass('hidden');
                        $('#input-container-user_id').removeClass('hidden');
                    }
                });

               

                $('input[name="schedule"]').change( function() {
                    if( $(this).val() == 'now'  ){
                        $('#input-container-date').addClass('hidden');
                        $('#input-container-time').addClass('hidden');
                    }

                    if( $(this).val() == 'scheduled'  ){
                        $('#input-container-date').removeClass('hidden');
                        $('#input-container-time').removeClass('hidden');
                    }
                });

                $('select[name="audience"]').change();
                $('input[name="schedule"]').change();
            });

        </script>

    @endsection
