<!-- layout -->
    @extends('OneSignalView::notification_base')

    @section('notification-content')
    	@component('WCView::general.components.panels.main')
            @slot('title','One Signal Sent Notifications')
            @slot('content')
            <div> Total Notifications: {{ $data->total_count }} </div>
            <ul class="pagination">
                @for( $i=1 ; $i <= $data->page_number ; $i++)
                    @if( $data->current_page == $i ) 
                        <li class="active"><a>{{ $i }}</a></li>
                    @else 
                        <li ><a href="{{ route('notifications.list', [ 'page' => $i ]) }}">{{ $i }}</a></li>
                    @endif                       
                @endfor
            </ul>
            <table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Status</th>
                    <!-- <th>ID</th> -->
                    <th>Title</th>
                    <th>Message</th>
                    <th>Sent At</th>
                    <th>Sent To</th>
                    <th>Clicked</th>
                </tr>
            </thead>
            <tbody>
            @foreach( $data->notifications as $index => $notification)
                @php
                    $time = \Carbon\Carbon::createFromTimestamp($notification->send_after);
                    $now = \Carbon\Carbon::now();
                @endphp
                <tr>
                    <td>{{ $data->start_index + $index }}</td>
                    <td>
                        @if ( $time > $now || $notification->remaining > 0 ) 
                            <span class="label label-primary">Scheduled</span>
                        @else
                            <span class="label label-success">Sent</span>
                        @endif

                    </td>
                    <!-- <td> {{ $notification->id }} </td> -->
                    <td> 
                        @if( isset($notification->headings) )
                            @foreach( $notification->headings as $heading )
                                {{ $heading  }} 
                            @endforeach
                        @endif
                    </td>
                    <td> 
                        @if( isset($notification->contents) )
                            {{ $notification->contents->en }} 
                        @endif
                    </td>
                    <td> 
                        
                        @if( isset($notification->send_after) )
                            {{ $time->toDateTimeString()  }} 
                            <br>
                            {{ $time->diffForHumans($now) }} 
                        @endif
                    </td>
                    <td> 
                        @if( isset($notification->successful) )
                             {{ $notification->successful }} 
                        @endif
                    </td>
                    <td> 
                        @if( isset($notification->converted) )
                             {{ $notification->converted }} 
                        @endif
                    </td>

                </tr>


            @endforeach
        	</tbody>
            </table>

            <ul class="pagination">
                @for( $i=1 ; $i <= $data->page_number ; $i++)
                    @if( $data->current_page == $i ) 
                        <li class="active"><a>{{ $i }}</a></li>
                    @else 
                        <li ><a href="{{ route('notifications.list', [ 'page' => $i ]) }}">{{ $i }}</a></li>
                    @endif                       
                @endfor
            </ul>
        @endslot
        @endcomponent
    @endsection


