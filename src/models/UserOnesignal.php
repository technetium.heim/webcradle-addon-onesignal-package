<?php

namespace OneSignal\models;

use App\Models\AppCradleModel;

class UserOnesignal extends AppCradleModel
{
    protected $table = 'user_onesignals';   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id, onesignal_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
