<?php

namespace OneSignal\foundation;

trait UserOnesignalTrait
{
    public function onesignals() 
    {
        return $this->hasMany('OneSignal\models\UserOnesignal');
    }

}
