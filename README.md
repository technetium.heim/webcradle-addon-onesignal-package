## Dependency
Web Cradle One Signal is dependence on On Laravel

## Private Package
- to use webcradle-addon, it required login user and password / SSH to the git repositories for download.

## How to Install WC One Signal Package
1. @composer.json > "repositories", add:
           
    { "type": "vcs", "url": "git@gitlab.com:technetium.heim/webcradle-addon-onesignal-package.git" }
2. @composer.json> "require" , add:
`"technetium.heim/OneSignal": "dev-master"`
3. @cmd, run  `composer update technetium.heim/OneSignal`
4. It will ask for username and password for the git repo. Fill in and continue.
5.  @ root\config\app.php, 
  - add under providers: `OneSignal\OneSignalServiceProvider::class,`
  - add under alias: 
 `'OneSignal' => OneSignal\facades\OneSignalFacade::class,`

6.  @cmd prompt, run `php artisan vendor:publish --tag=onesignal` to initiate copy one signal config files.

## Configuration

You need to fill in `onesignal.php` file that is found in your applications `config` directory.
`app_id` is your *OneSignal App ID* and `rest_api_key` is your *REST API Key*.

## Usage

### Sending a Notification To All Users

You can easily send a message to all registered users with the command

    OneSignal::sendNotificationToAll("Title","Some Message");
    OneSignal::sendNotificationToAll("Title","Some Message", $url);
    OneSignal::sendNotificationToAll("Title","Some Message", $url, $data);
    OneSignal::sendNotificationToAll("Title","Some Message", $url, $data, $buttons);
    OneSignal::sendNotificationToAll("Title","Some Message", $url, $data, $buttons,$schedule);
    
`$url` , `$data` , `$buttons` and `$schedule` fields are exceptional. If you provide a `$url` parameter, users will be redirecting to that url.
    

### Sending a Notification To A Specific User

After storing a user's tokens in a table, you can simply send a message with

    OneSignal::sendNotificationToUser("Title","Some Message", $userId);
    OneSignal::sendNotificationToUser("Title","Some Message", $userId, $url);
    OneSignal::sendNotificationToUser("Title","Some Message", $userId, $url, $data);
    OneSignal::sendNotificationToUser("Title","Some Message", $userId, $url, $data, $buttons);
    OneSignal::sendNotificationToUser("Title","Some Message", $userId, $url, $data, $buttons,$schedule);
    
`$userId` is the user's unique id where he/she is registered for notifications. Read https://documentation.onesignal.com/docs/web-push-tagging-guide for additional details.
`$url` , `$data` , `$buttons` and `$schedule` fields are exceptional. If you provide a `$url` parameter, users will be redirecting to that url.


### Sending a Notification To Segment

You can simply send a notification to a specific segment with

    OneSignal::sendNotificationToSegment("Title","Some Message", $segment);
    OneSignal::sendNotificationToSegment("Title","Some Message", $segment, $url);
    OneSignal::sendNotificationToSegment("Title","Some Message", $segment, $url, $data);
    OneSignal::sendNotificationToSegment("Title","Some Message", $segment, $url, $data, $buttons);
    OneSignal::sendNotificationToSegment("Title","Some Message", $segment, $url, $data, $buttons,$schedule);
    
`$url` , `$data` , `$buttons` and `$schedule` fields are exceptional. If you provide a `$url` parameter, users will be redirecting to that url.

### Sending a Custom Notification

You can send a custom message with 

    OneSignal::sendNotificationCustom($parameters);
    
    ### Sending a Custom Notification
### Sending a async Custom Notification
You can send a async custom message with 

    OneSignal::async()->sendNotificationCustom($parameters);
    
Please refer to https://documentation.onesignal.com/reference for all customizable parameters.


